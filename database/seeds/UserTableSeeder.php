<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $users = factory(User::class)->times(50)->make();
        User::insert($users->makeVisible(['password','remember_token'])->toArray());

        $user_1 = User::find(1);
        $user_1->name = 'liwei';
        $user_1->email = '1515@qq.com';
        $user_1->password = bcrypt('123456');
        $user_1->is_admin = 1;
        $user_1->activated = 1;
        $user_1->save();
        $user_2 = User::find(2);
        $user_2->name = 'joy';
        $user_2->email = 'joy@qq.com';
        $user_2->password = bcrypt('123456');
        $user_2->activated = 1;
        $user_2->save();
    }
}
